class TaskQueue {
  constructor () {
    /**
     * @type {Array.<Function>}
     * @private
     */
    this._tasks = [];
  }

  /**
   * @private
   */
  async _run () {
    if (this._tasks.length === 0) {
      return; // stop process
    }
    await this._tasks[0]();
    this._tasks.shift();
    if (this._tasks.length > 0) {
      setTimeout(() => {
        this._run();
      }, 0);
    }
  }

  /**
   * @param {Function} func
   * @public
   */
  push (func) {
    this._tasks.push(func);
    if (this._tasks.length === 1) {
      this._run();
    }
  }

  /**
   * @public
   */
  clean () {
    this._tasks.length = 0;
  }
}

module.exports = TaskQueue;
