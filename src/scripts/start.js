/* eslint-disable no-console */
const Application = require('../Application.js');
const config = require('../config')();

(async () => {
  try {
    const app = new Application(config);
    await app.run();
    console.log(`Server is running on port ${app.config.get('port')}`);
  } catch (err) {
    console.error(`Failed to run cause of "${err.message}"`);
    process.exit();
  }
})();
