const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  createdAt: {
    type: Date,
    required: true,
  },
  expiresAt: {
    type: Date,
    required: true,
  },
  body: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },
});

schema.index({ expiresAt: 1 }, { expireAfterSeconds: 0 });

schema.methods.toJSON = function () {
  const obj = this.toObject();
  this.id = this._id.toString();
  delete this._id;
  return obj;
};

/**
 * @param {Object.<string, *>}
 * @returns {Promise.<boolean>}
 */
schema.statics.hasExactDuplicate = async function (doc = {}) {
  if (Object.keys(doc).length === 0) {
    throw Error('Bad param: "doc" must contain at least one field');
  }
  const duplicate = await this.findOne({
    body: doc,
    expiresAt: { $gt: new Date() },
  });
  return Boolean(duplicate);
};

/**
 * @param {Object.<string, *>}
 * @returns {Promise.<boolean>}
 */
schema.statics.hasPartialDuplicate = async function (doc = {}) {
  if (Object.keys(doc).length === 0) {
    throw Error('Bad param: "doc" must contain at least one field');
  }
  const filters = Object
    .entries(doc)
    .map(([key, value]) => ({ [`body.${key}`]: value }));
  const duplicate = await this.findOne({
    $or: filters,
    expiresAt: { $gt: new Date() },
  });
  return Boolean(duplicate);
};

/**
 * @param {number} interval Seconds from now until the expire date
 * @returns {Promise.<number>}
 */
schema.statics.countExpiring = async function (interval) {
  if (typeof interval !== 'number' || !interval || interval < 0) {
    throw Error('Bad param: "interval" must be a positive number');
  }
  const expirationDate = new Date();
  expirationDate.setSeconds(expirationDate.getSeconds() + interval);
  const count = await this.countDocuments({
    expiresAt: { $lte: expirationDate },
  });
  return count;
};

module.exports = mongoose.model('Document', schema);
