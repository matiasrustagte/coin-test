const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  date: {
    type: Date,
    required: true,
  },
  unique: {
    type: Boolean,
    required: true,
  },
});

schema.methods.toJSON = function () {
  const obj = this.toObject();
  delete this._id;
  return obj;
};

schema.index({ date: -1 });

/**
 * @typedef {Object} DayReport
 * @property {string} day In format "Y-M-D"
 * @property {number} unique Total count of unique items
 * @property {number} notUnique Total count of not unique items
 * @property {number} total
 */

/**
 * @param {Date=} from
 * @param {Date=} to
 * @returns {promise.<Array.<DayReport>>}
 * @public
 */
schema.statics.getStatistics = async function (from, to) {
  if (from && (!(from instanceof Date) || Number.isNaN(from.getTime()))) {
    throw Error('Bad param: "from" must be a valid date or empty');
  }
  if (to && (!(to instanceof Date) || Number.isNaN(to.getTime()))) {
    throw Error('Bad param: "to" must be a valid date or empty');
  }
  const filter = {};
  if (from) {
    filter['$gte'] = from;
  }
  if (to) {
    filter['$lte'] = to;
  }
  const pipeline = [];
  if (Object.keys(filter) > 0) {
    pipeline.push({
      $match: {
        date: filter,
      },
    });
  }
  const result = await this.aggregate([
    ...pipeline,
    ...[
      {
        $group: {
          _id: {
            $dateToString: {
              format: '%Y-%m-%d',
              date: '$date',
            },
          },
          unique: {
            $sum: {
              $cond: ['$unique', 1, 0],
            },
          },
          total: {
            $sum: 1,
          },
        },
      },
      {
        $project: {
          _id: false,
          day: '$_id',
          unique: true,
          notUnique: {
            $subtract: ['$total', '$unique'],
          },
        },
      },
      {
        $sort: {
          day: 1,
        },
      },
    ],
  ]);
  return result;
};

module.exports = mongoose.model('Request', schema);
