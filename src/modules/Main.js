const koaBody = require('koa-body');
const Router = require('koa-router');
const TaskQueue = require('../utils/TaskQueue.js');

const parseBody = koaBody({
  urlencoded: false,
  multipart: false,
  json: true,
  text: false,
});

class Main {
  static async initialize (app) {
    // Collection of "unique" documents
    this.Document = app.models.Document;
    if (!this.Document) {
      throw Error('Model "Document" missing!');
    }

    // Collection of requests logs
    this.Request = app.models.Request;
    if (!this.Request) {
      throw Error('Model "Requests" missing!');
    }

    this._taskQueue = new TaskQueue();

    const router = new Router();
    router.post('/check', parseBody, this._checkUniqueness.bind(this));
    router.get('/report', this._getReport.bind(this));
    app.koa
      .use(router.routes())
      .use(router.allowedMethods());
  }

  static async destroy () {
    this._taskQueue.clean();
  }

  /**
   * @private
   */
  static async _checkUniqueness (ctx) {
    let { exact } = ctx.request.body;
    if (!ctx.request.body.doc) {
      ctx.throw(400, 'Property "doc" required');
    }
    let { ttl, ...doc } = ctx.request.body.doc;

    if (ttl === undefined) {
      ctx.throw(400, 'Property "ttl" required');
    }
    ttl = Number(ttl);
    if (Number.isNaN(ttl) || ttl <= 0) {
      ctx.throw(400, 'Property "ttl" must be a positive number');
    }

    if (Object.keys(doc).length === 0) {
      ctx.throw(400, 'Property "doc" must contain at least one field except "ttl"');
    }

    exact = exact !== undefined ? Boolean(exact) : false;

    // date must be captured before all the async code
    const date = new Date();

    const unique = await this.analyze(doc, { date, exact, ttl });
    ctx.body = unique;

    if (this.Request) {
      setTimeout(async () => {
        try {
          const log = new this.Request({
            date,
            unique,
          });
          await log.save();
        } catch (err) {
          console.error(err); // @todo: use winston instead
        }
      });
    }
  }

  static async _getReport (ctx) {
    const expiring = await this.Document.countExpiring(3600);
    const requests = await this.Request.getStatistics();
    const requestsHTML = requests
      .map(({ day, unique, notUnique }) => {
        return `<tr><td>${day}</td><td>${unique}</td><td>${notUnique}</td></tr>`;
      })
      .join('');
    ctx.body = `<!doctype html>
      <head>
        <meta charset="utf-8"><title>Отчёт</title>
        <style>
          table, td, th { border: solid 1px #666; }
          table { border-collapse: collapse; }
          td, th { padding: 0.5em; text-align: center; vertical-align: center; }
        </style>
      </head>
      <body>
        <h1>Отчёт</h1>
        <h2>Документы</h2>
        <p>Количество документов, срок годности которых истекает</p>
        <table>
          <thead><th>через 1 час</th></thead>
          <tbody><tr><td>${expiring}</td></tr></tbody>
        </table>
        </h2>
        <h2>Запросы</h2>
        <table>
          <thead><th>Дата</th><th>Уникальные</th><th>Неуникальные</th></thead>
          <tbody>${requestsHTML}</tbody>
        </table>
      </body>`;
  }

  /**
   * @param {Object} doc
   * @param {Object=} options
   * @param {Date=} [options.date=now]
   * @param {boolean=} [options.exact=false]
   * @return {Promise.<boolean>}
   * @public
   */
  static analyze (doc, { date = new Date(), exact = false, ttl = 1 } = {}) {
    return new Promise((resolve, reject) => {
      const task = async () => {
        try {
          const hasDuplicate = exact
            ? await this.Document.hasExactDuplicate(doc)
            : await this.Document.hasPartialDuplicate(doc);
          if (!hasDuplicate) {
            const newDoc = new this.Document({
              body: doc,
              createdAt: date,
              expiresAt: new Date(date.getTime() + ttl * 1000),
            });
            await newDoc.save();
          }
          const unique = !hasDuplicate;
          resolve(unique);
        } catch (err) {
          reject(err);
        }
      };
      this._taskQueue.push(task);
    });
  }
}

module.exports = Main;
