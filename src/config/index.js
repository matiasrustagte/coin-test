const nconf = require('nconf');
const path = require('path');

module.exports = (customConfigFile = '') => {
  nconf.argv({
    parseValues: true,
  });

  nconf.env({
    separator: '.',
    lowerCase: true,
    parseValues: true,
    // transform: (obj) => {  } // transform snake_case to camelCase
  });

  if (customConfigFile) {
    nconf.file('custom', customConfigFile);
  }

  const env = process.env.NODE_ENV || 'dev';
  if (env !== 'dev') {
    nconf.file('environmentDefaults', path.join(__dirname, `${env}.json`));
  }

  nconf.file('defaults', path.join(__dirname, 'dev.json'));

  return nconf;
};
