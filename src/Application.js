const Koa = require('koa');
const mongoose = require('mongoose');
const Main = require('./modules/Main.js');
const packageJSON = require('../package.json');
const models = require('./models/index.js');

const modules = [Main]; // Массив на случай, если ещё добавятся модули

// @todo Add logger Winston
class Application {
  constructor (config) {
    this.config = config;
    this.koa = null;
    this.server = null;
    this.modules = {};
    this.models = {};
  }

  async run () {
    // 1. Prepare Mongoose and models
    this.models = models;
    await mongoose.connect(this.config.get('db:url'), {
      useNewUrlParser: true,
      autoIndex: this.config.get('db:autoIndex'), // soft rebuild indexes
      useCreateIndex: true,
    });

    // 2. Prepare Koa application
    this.koa = new Koa();
    this.koa.use(async (ctx, next) => {
      if (ctx.request.method === 'GET' && ctx.request.path === '/') {
        ctx.body = {
          name: packageJSON.name,
          version: packageJSON.version,
        };
        return;
      }
      await next();
    });

    // 3. Load business logic
    for (const moduleClass of modules) {
      if (moduleClass.initialize) {
        await moduleClass.initialize(this);
      }
      this.modules[moduleClass.name] = moduleClass;
    }

    // 4. Run server
    const port = this.config.get('port');
    this.server = this.koa.listen(port);
    console.log(`Listening port ${port}`);
  }

  // Opposite to the "run" method
  async stop () {
    for (const moduleClass of Object.values(this.modules).reverse()) {
      if (moduleClass.destroy) {
        await moduleClass.destroy();
      }
    }
    this.modules = {};

    await mongoose.disconnect();
    this.models = {};

    this.server.close();
    this.server = null;

    this.koa = null;
  }
}

module.exports = Application;
