const request = require('supertest');
const Application = require('../src/Application.js');
const config = require('../src/config')();

const wait = (time) => {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, time * 1000);
  });
};

let app = null;

const A = { tree: 'ash', color: 'yellow', shape: 'circle', digit: 8, ttl: 1 };
const B = { fruit: 'pineapple', mood: 'blue', music: 'launge', ttl: 1 };
const C = { tree: 'willow', color: 'green', shape: 'circle', digit: 8, ttl: 1 };

beforeAll(async () => {
  app = new Application(config);
  await app.run();
  await app.models.Document.remove();
  await app.models.Request.remove();
});

afterAll(async () => {
  await app.models.Document.remove();
  await app.models.Request.remove();
  await app.stop();
  app = null;
});

describe('checkUniqueness', () => {
  test('First document is unique', async () => {
    const response = await request(app.server)
      .post('/check')
      .send({
        doc: A,
        exact: true,
      })
      .expect(200);
    expect(response.body).toBe(true);
  });

  test('Another document is unique', async () => {
    const response = await request(app.server)
      .post('/check')
      .send({
        doc: B,
        exact: true,
      })
      .expect(200);
    expect(response.body).toBe(true);
  });

  test('Exact duplicate is not unique', async () => {
    const response = await request(app.server)
      .post('/check')
      .send({
        doc: A,
        exact: true,
      })
      .expect(200);
    expect(response.body).toBe(false);
  });

  test('Non-exact duplicate is not unique', async () => {
    const response = await request(app.server)
      .post('/check')
      .send({
        doc: C,
        exact: false,
      })
      .expect(200);
    expect(response.body).toBe(false);
  });

  test('Non-exact duplicate becomes unique after the previous document expires', async () => {
    await wait(1);
    const response = await request(app.server)
      .post('/check')
      .send({
        doc: C,
        exact: false,
      })
      .expect(200);
    expect(response.body).toBe(true);
  });
});