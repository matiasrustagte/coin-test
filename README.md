# coin-test

## Запуск
```
npm start
```
или
```
docker-compose up
```

Запущенное приложение доступно по адресу http://localhost:5555

## Зависимости
* Node 10.x
* MongoDB 4.x

## API

### POST /check
**Request content type**

application/json

**Request body**
```
{
    "doc": {
        <some fields>, // разные поля, но не менее 1 штуки
        "ttl": <positive number> // срок актуальности документа в секундах
    },
    "exact": <true|false> // точное или частичное совпадение
}
```
**Response content type**

application/json

**Response body**
```
true|false
```

### GET /report
**Request parameters**

No parameters

**Response content type**

text/html