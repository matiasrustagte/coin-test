FROM node:10-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install --production
COPY src /app/src
EXPOSE 5555
CMD ["node", "src/scripts/start.js"]